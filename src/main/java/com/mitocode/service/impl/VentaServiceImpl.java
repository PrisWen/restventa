package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IVentaDAO;
import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;


@Service
public class VentaServiceImpl implements IVentaService{
	@Autowired
	private IVentaDAO dao;
	
	@Override
	public Venta registrar(Venta venta) {
		// TODO Auto-generated method stub
		venta.getDetalleventa().forEach(x -> x.setVenta(venta));		
		return dao.save(venta);
	}

	@Override
	public Venta listarId(int idVenta) {
		// TODO Auto-generated method stub
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
