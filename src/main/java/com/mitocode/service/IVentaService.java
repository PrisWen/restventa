package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Venta;

public interface IVentaService {
	Venta registrar(Venta Venta);

	Venta listarId(int idVenta);

	List<Venta> listar();
}
