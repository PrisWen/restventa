package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestVentaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestVentaApplication.class, args);
	}
}
